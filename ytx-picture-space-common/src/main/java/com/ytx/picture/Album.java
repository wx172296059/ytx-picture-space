package com.ytx.picture;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 相册实体,用于图片存放
 * <p>
 * date : 2017/12/27
 * time : 10:47
 * </p>
 *
 * @author Nero
 */
@Data
public class Album implements Serializable {

    /**
     * 主键ID
     */
    private Long id;
    /**
     * 相册的名称
     */
    private String name;
    /**
     * 相册归属账号id
     */
    private Long accountId;
    /**
     * 相册的状态
     *
     * @see com.ytx.picture.Album.AlbumStatus
     */
    private Integer status;
    /**
     * 上级ID
     */
    private Long parentId;
    /**
     * 是否叶子节点
     */
    private Integer leaf;
    /**
     * 路由id集合，当前字段表示从根节点到当前节点的路由
     * 示例：&0&2&5&98&....
     */
    private String routeIds;
    /**
     * 创建时间
     */
    private Date createdAt;
    /**
     * 修改时间
     */
    private Date updatedAt;

    /**
     * 相册状态枚举
     */
    @AllArgsConstructor
    public enum AlbumStatus {

        NORMAL(1, "正常"), RECYCLEBIN(0, "回收站"), DELETED(-1, "彻底删除");

        private Integer index;

        private String name;
    }
}
