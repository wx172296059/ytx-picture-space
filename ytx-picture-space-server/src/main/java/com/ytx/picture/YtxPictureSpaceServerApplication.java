package com.ytx.picture;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YtxPictureSpaceServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(YtxPictureSpaceServerApplication.class, args);
	}
}
